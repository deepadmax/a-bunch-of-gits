# A Bunch of Gits

Manage multiple Git profiles.

- Switch between different configurations for global name and email.
- Clone a repository with the local name and email of a particular profile.

## Configure profiles

Profiles are stored in `~/.config/gits/profiles` as follows.

```
profile-name: Firstname Surname <email@address.wow>
second-profile: Another Identity <badmail@geemail.wow>
```

## Commands

- `gits get`: Get current global name and email.
- `gits get <profile>`: Get name and email of the given profile.
- `gits set <profile>`: Update the global name and email to those of the given profile.
- `gits clone <profile> [...]`: Clone a repository and set the local name and email to those of the given profile. All arguments following the profile name will be passed on to `git clone`.